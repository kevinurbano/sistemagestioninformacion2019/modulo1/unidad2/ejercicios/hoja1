﻿USE ciclistas;
/* Modulo 1 Unidad 2 */
/* Consulta de seleccion 1 */

/* Consulta 1 */
SELECT
  DISTINCT c.edad -- proyeccion
FROM 
  ciclista c; -- tabla que utilizo

/* Consulta 2 */
SELECT 
  DISTINCT c.edad 
FROM 
  ciclista c 
WHERE 
  c.nomequipo='Artiach';

/* Consulta 3 */
SELECT 
  DISTINCT c.edad 
FROM 
  ciclista c WHERE c.nomequipo IN ('Artiach','Amore Vita');

-- Con UNION.
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Artiach'
UNION
SELECT DISTINCT c.edad FROM ciclista c WHERE c.nomequipo='Amore Vita';

/* Consulta 4 */
-- Con Between
SELECT c.dorsal FROM ciclista c WHERE  c.edad NOT BETWEEN 25 AND 30;

-- Con OR
SELECT c.dorsal FROM ciclista c WHERE c.edad < 25 OR c.edad > 30;

-- Con Union
  -- c1: Listar ciclistas menores de 25
  SELECT c.dorsal FROM ciclista c WHERE c.edad<25;
  -- c2: Listar ciclistas mayores de 30
  SELECT c.dorsal FROM ciclista c WHERE c.edad>30;
  -- final
  SELECT c.dorsal FROM ciclista c WHERE c.edad<25
  UNION
  SELECT c.dorsal FROM ciclista c WHERE c.edad>30;

/* Consulta 5 */
SELECT c.dorsal FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 AND c.nomequipo='Banesto';

/* Consulta 6 */
SELECT DISTINCT c.nombre FROM ciclista c WHERE CHAR_LENGTH(c.nombre)>8;

/* Consulta 7 */
SELECT nombre,dorsal,UPPER(nombre) nombre_mayus FROM ciclista;

/* Consulta 8 */
SELECT DISTINCT l.dorsal FROM lleva l WHERE l.código='MGE';

/* Consulta 9 */
SELECT DISTINCT p.nompuerto FROM puerto p WHERE p.altura>1500;

/* Consulta 10 */
SELECT DISTINCT p.dorsal FROM puerto p WHERE p.pendiente>8 OR p.altura BETWEEN 1800 AND 3000;

/* Consulta 11 */
SELECT DISTINCT P.dorsal FROM puerto p WHERE p.pendiente>8 AND p.altura BETWEEN 1800 AND 3000;